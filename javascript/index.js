const tempo = 1000;

comecarLoucura();

function comecarLoucura() {
    startmudarDeCor(tempo);
}

function startmudarDeCor(tempo) {

    let col = document.getElementsByClassName("col");
    for (let i = 0; i < col.length; i++) {
        col[i].style.backgroundColor = getRandomColor();
    }

    setTimeout(() => {
        let col = document.getElementsByClassName("col");
        for (let i = 0; i < col.length; i++) {
            col[i].style.backgroundColor = getRandomColor();
        }
        startmudarDeCor(tempo);
    }, tempo);
}

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}