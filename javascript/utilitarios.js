const espaco = "<span style='opacity: 0'> só pra ocupar mais espalço essa pohha </span>";

const gridLinks = [{
        url: "telas/calculadora.html",
        title: "Calculadora",
        descricao: " &nbsp; Aqui temos uma calculadora básica" + espaco,
        imgSrc: "",
        imgAlt: "calculadora"
    },

    {
        url: "telas/porextenso.html",
        title: "Número por Extenso",
        descricao: " &nbsp; Gera números por extenso a partir de um valor",
        imgSrc: "",
        imgAlt: "Remover Acento"
    },

    {
        url: "telas/romano.html",
        title: "Número Romano",
        descricao: " &nbsp; Converte Número Romanos e vice-versa",
        imgSrc: "",
        imgAlt: "Número Romano"
    },

    {
        url: "telas/validadorDocumento.html",
        title: "Validador de Documento",
        descricao: " &nbsp; Validador de CPF e CNPJ" + espaco,
        imgSrc: "",
        imgAlt: "validador"
    },
    {
        url: "telas/invertertexto.html",
        title: "Inversão de Texto",
        descricao: " &nbsp; Inversão de texto dinâmica",
        imgSrc: "",
        imgAlt: "validador"
    },

    {
        url: "telas/ordernarTexto.html",
        title: "Ordem Alfabética",
        descricao: " &nbsp; Ordenação de texto por ordem alfabética",
        imgSrc: "",
        imgAlt: "Ordem Alfabética"
    },

    {
        url: "telas/contadorTexto.html",
        title: "Contador de Texto",
        descricao: " &nbsp; Contador de texto semi-convencional",
        imgSrc: "",
        imgAlt: "Contador texto"
    },

    {
        url: "telas/removerAcento.html",
        title: "Remover Acento",
        descricao: " &nbsp; Removedor de acentos em um texto",
        imgSrc: "",
        imgAlt: "Remover Acento"
    },

];

const gridNavbarLinks = [
    { text: "Utilitários", link: `index.html`, id: "utilitarios", icon: "", filhos: null },
    {
        text: "Números",
        id: "numeroId",
        icon: "",
        filhos: [
            { id: "calcId", link: `telas/calculadora.html`, text: "Calculadora Básica", icon: "" },
            { id: "numberExtId", link: `telas/porextenso.html`, text: "Número por Extenso", icon: "" },
            { id: "numRomId", link: `telas/romano.html`, text: "Converter Número Romanos", icon: "" },
        ]
    },
    {
        text: "Textos",
        id: "textId",
        icon: "",
        filhos: [
            { id: "ordAlfId", link: `telas/ordernarTexto.html`, text: "Ordem Alfabética", icon: "" },
            { id: "inverTextId", link: `telas/invertertexto.html`, text: "Inverter Texto", icon: "" },
            { id: "remAcenId", link: `telas/removerAcento.html`, text: "Remover Acento", icon: "" },
            { id: "contTextId", link: `telas/contadorTexto.html`, text: "Contador texto", icon: "" },
        ]
    },
    { text: "Validador Documento", link: `telas/validadorDocumento.html`, id: "documentoId", icon: "", filhos: null },
];


function carregarNavbarStatic(idConteudo, imageDefault, currentFolder) {
    if (currentFolder == undefined)
        currentFolder = false;

    localPasta = currentFolder;
    console.log(currentFolder);
    carregarNavbarLinksStatic(gridNavbarLinks, idConteudo, imageDefault);
}

function carregarGridContentLinksStatic(idConteudo) {
    carregarGridLinksStatic(gridLinks, idConteudo);
}

function carregarGridLinksStatic(links, idConteudo) {
    let conteudo = "";
    links.forEach(link => {
        let img = link.imgSrc ? `<img class="card-img-top" src="${link.imgSrc}" alt="${link.imgAlt}">` : '';
        conteudo += `
            <div class="col card link shadow" onclick="navegar('${link.url}')">
                ${img}
                <div class="card-body">
                    <h3 class="card-title">${link.title}</h3>
                    <h4 class="card-text">${link.descricao}</h4>
                </div>
            </div>
        `;
    });
    mudarHtml(idConteudo, conteudo);
}

function carregarNavbarLinksStatic(links, idConteudo, imageDefault) {
    let conteudo = "";
    links.forEach(link => {
        conteudo += getEstruturaNavbarLinkStatic(link, imageDefault);
    });
    mudarHtml(idConteudo, conteudo);
}

function getEstruturaNavbarLinkStatic(link, imageDefault) {
    let getImage = (url) => { return url ? `<img width='30px' src='${url}' /> &nbsp; ` : ""; };
    if (imageDefault) {
        getImage = (url) => { return url ? `<img width='30px' src='../${url}' /> &nbsp; ` : ""; };
    }
    let filhos = "";
    if (link.filhos) {

        link.filhos.forEach(filho => {
            filhos += `<a class="dropdown-link"href="${filho.link}" > ${getImage(filho.icon)} ${filho.text} </a>`;
        });

        return `
        <div class="nav-item dropdown">
            <span class="dropbtn" id="${link.id}" > ${getImage(link.icon)} ${link.text} <img src="imagens/icons/seta-para-baixo.png" width="15px" alt=""> </span>
            <div id="myDropdown" class="dropdown-content">
                ${filhos}
            </div>
        </div>
        `;
    } else {
        return `
        <div class="nav-item" id="${link.id}" >
            <a class="link"href="${link.link}">${getImage(link.icon)} ${link.text}</a>
        </div>
        `;
    }
}

function mudarHtml(idConteudo, novoConteudo) {
    document.getElementById(idConteudo).innerHTML = novoConteudo;
}

function navegar(pagina) {
    window.location.href = pagina;
}