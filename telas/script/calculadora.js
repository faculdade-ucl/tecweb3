let operacao1 = 0;
let operacao2 = 0;
let operacao = '';
let historico = [""];
let igualRepetido = false;

function escreverNumero() {
    document.getElementById("exibir").innerHTML = document.getElementById("pegavalor").value
}

function pegaoperador(valor) {

    if (operacao) operacao1 = calcular(operacao1, pegarPrimeiraOperacao(), operacao);
    else operacao1 = pegarPrimeiraOperacao();

    console.log(operacao1);

    operacao = valor;
    historico[historico.length - 1] += pegarPrimeiraOperacao() + " " + operacao + " ";
    document.getElementById("exibir").innerHTML = "";
    igualRepetido = false;

}

function pegavalor(valor) {
    document.getElementById("exibir").innerHTML += valor;
}

function pegarPrimeiraOperacao() {
    let valor = document.getElementById("exibir").innerHTML;
    return valor;
}

function calcula() {
    let valorCalculado = calcular(operacao1, pegarPrimeiraOperacao(), operacao);
    if (igualRepetido) {
        historico[historico.length - 1] += pegarPrimeiraOperacao() + " " + operacao + " " + operacao1 + " = " + valorCalculado;
    } else {
        historico[historico.length - 1] += pegarPrimeiraOperacao() + " = " + valorCalculado;
    }
    let historicoStr = "";
    historico.forEach(element => { historicoStr += element + "<br />"; });
    document.getElementById("historico").innerHTML = historicoStr;
    document.getElementById("exibir").innerHTML = valorCalculado;
    historico.push("");
    igualRepetido = true;
    console.log(historico)
}

function limpa() {
    document.getElementById("exibir").innerHTML = "";
    operacao1 = 0;
    operacao2 = 0;
    operacao = '';
    historico = [""];
    document.getElementById("historico").innerHTML = "";
}

function calcular(n1, n2, operacao) {
    if (operacao == "+") {
        return parseFloat(n1) + parseFloat(n2);
    }
    if (operacao == "-") {
        return parseFloat(n1) - parseFloat(n2);
    }
    if (operacao == "x") {
        return parseFloat(n1) * parseFloat(n2);
    }
    if (operacao == "/") {
        return parseFloat(n1) / parseFloat(n2);
    }

}