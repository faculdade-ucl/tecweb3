// caracteres, palavras, espaços, linhas, vogais e números.
function contar() {
    const valor = document.getElementById("numeroContar").value;
    let caracteres = contarCaracteres(valor);
    let Palavras = contarPalavras(valor);
    let Espacos = contarEspacos(valor);
    let Linhas = contarLinhas(valor);
    let Vogais = contarVogais(valor);
    let numeros = contarNumeros(valor);
    document.getElementById("numeroContador").innerHTML = `
    caracteres: ${caracteres} <br />
    palavras: ${Palavras} <br />
    espaços: ${Espacos} <br />
    linhas: ${Linhas} <br />
    vogais: ${Vogais} <br />
    números: ${numeros} <br />
    `;

}

function contarCaracteres(valor) {
    console.log(valor);
    return (valor.toLowerCase().match(/[a-z]/g) || []).length;
}

function contarPalavras(valor) {
    return valor.split(" ").filter(valor => { return (valor.toLowerCase().match(/[a-z]/g) || []).length > 0 }).length;
}

function contarEspacos(valor) {
    return valor.split(" ").length;
}

function contarLinhas(valor) {
    return valor.split("\n").length;
}

function contarVogais(valor) {
    return (valor.toLowerCase().match(/[aeiou]/g) || []).length;
}

function contarNumeros(valor) {
    return (valor.toLowerCase().match(/[0-9]/g) || []).length;
}