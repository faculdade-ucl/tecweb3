let ordenarPorLetra = true;

function organizarTexto() {
    let value = document.getElementById("texto").value;
    let ordenar = ordenarPorLetra ? " " : "\n";
    let valorParaBotar = value.split(ordenar).sort().join(ordenar == "\n" ? "<br />" : " ");
    console.log(valorParaBotar);
    document.getElementById("textoOrdenado").innerHTML = valorParaBotar;
}

function trocarOrdem() {
    document.getElementById("textoOrdenado").innerHTML = "";
    var radios = document.getElementsByName('ordem');

    for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
            ordenarPorLetra = radios[i].value == "palavra";
        }
    }
}