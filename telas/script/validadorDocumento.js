let documento = "cpf";

function validarTexto() {
    let valor = document.getElementById('texto').value.trim();
    console.log(documento);
    if (documento == "cpf") {
        if (!validarPontuacaoCpf(valor)) {
            document.getElementById('textoValidado').innerHTML = "Cpf não validado: pontuação inválida";
            return;
        }
    } else if (documento == "cnpj") {
        if (!validarPontuacaoCnpj(valor)) {
            document.getElementById('textoValidado').innerHTML = "Cnpj não validado: pontuação inválida";
            return;
        }
    }

    valor = removerAcento(valor);
    if (documento == "cpf") {
        document.getElementById('textoValidado').innerHTML = validarCpf(valor);
    } else {
        document.getElementById('textoValidado').innerHTML = validarCnpj(valor);
    }
}

function removerAcento(valor) {

    const acentos = [",", ".", "-", "/"];
    valor = valor.replace(".", "").replace("-", "").replace("/", "");
    acentos.forEach(acento => {
        valor = valor.replace(acento, "");
        let valorTemp = valor.replace(acento, "");
        while (valorTemp != valor) {
            valor = valor.replace(acento, "");
            valorTemp = valor.replace(acento, "");
        }
    });
    return valor;
}

function trocarOrdem() {
    var radios = document.getElementsByName('ordem');
    document.getElementById('textoValidado').innerHTML = "";

    for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
            documento = radios[i].value;
        }
    }
}

function validarCpf(valor) {

    var Soma;
    var Resto;
    Soma = 0;
    if (valor == "00000000000") return "CPF não validado: pontuação inválida";

    for (i = 1; i <= 9; i++) Soma = Soma + parseInt(valor.substring(i - 1, i)) * (11 - i);
    Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11)) Resto = 0;
    if (Resto != parseInt(valor.substring(9, 10))) return "Cpf não validado: pontuação inválida";

    Soma = 0;
    for (i = 1; i <= 10; i++) Soma = Soma + parseInt(valor.substring(i - 1, i)) * (12 - i);
    Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11)) Resto = 0;
    if (Resto != parseInt(valor.substring(10, 11))) return "CPF validado com sucesso!";
    return "CPF validado com sucesso!";
}

// 176.187.067-73 - 3-7-11
function validarPontuacaoCpf(valor) {
    // 19006703729
    if (valor.length != 14 && valor.length != 11) {
        return false;
    } else {
        if (valor.length == 14) {
            if (valor[3] == '.' && valor[7] == '.' && valor[11] == '-') {
                return true
            }
        }
    }
    return true
}

// 13.983.929/0001-20 - 2-6-/10-(-)15
function validarPontuacaoCnpj(valor) {
    if (valor.length != 18 && valor.length != 14) return false; // verifica tamanho
    if (valor.indexOf(".") != -1) {
        return "fim: " + (valor[2] == "." && valor[6] == "." && valor[10] == "/" && valor[15] == "-"); // verifica as posições dos pontos
    }

    return true;
}

function validarCnpj(valor) {
    const msgNaoValidado = "Não validado";
    const msgValidado = "Cnpj validado";
    if (valor.length != 14) return msgNaoValidado;

    let valoresInvalidos = [];

    for (let i = 0; i > 10; i++) { valoresInvalidos.push(("" + i + "") * 14); }

    if (valoresInvalidos.indexOf(valor) != -1) return msgNaoValidado;

    let tamanho = valor.length - 2
    let digitos = valor.substring(tamanho);
    let numeros = valor.substring(0, tamanho);
    let soma = 0;
    let pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2) pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;

    if (resultado != digitos.charAt(0)) return msgNaoValidado;

    tamanho = tamanho + 1;
    numeros = valor.substring(0, tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2) pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1))
        return msgNaoValidado;

    return msgValidado;
}